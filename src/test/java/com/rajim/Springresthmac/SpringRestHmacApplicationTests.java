package com.rajim.Springresthmac;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rajim.Springresthmac.client.ApiClient;
import com.rajim.Springresthmac.server.model.Foo;
import com.rajim.Springresthmac.server.model.FooResponseWrapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.WebApplicationContext;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringRestHmacApplicationTests {


	private ApiClient client;

	@Value("${local.server.port}")
	private int port;

	@Before
	public void setup() {
		int port = this.port;
		client = new ApiClient("localhost", port);
		client.init();
	}




	@Test
	public void testEcho() {
		client.setCredentials("user", "password");
		Foo request = new Foo(randomAlphanumeric(5));
		final FooResponseWrapper response = client.echo(request, FooResponseWrapper.class);
		assertNotNull(response);
		assertEquals(request, response.getData());
	}

	@Test
	public void testFailWhenPasswordIsBad() {
		client.setCredentials("user", "bad password");
		Foo request = new Foo(randomAlphanumeric(5));
		try {
			client.echo(request, FooResponseWrapper.class);
			fail("Http Error 403 is expected");
		} catch (HttpClientErrorException e) {
			assertThat(e.getStatusCode(), is(HttpStatus.FORBIDDEN));
		}
	}

	@Test
	public void testFailWhenUserIsUnknown() {
		client.setCredentials("unknownUser", "any password");
		Foo request = new Foo(randomAlphanumeric(5));
		try {
			client.echo(request, FooResponseWrapper.class);
			fail("Http Error 403 is expected");
		} catch (HttpClientErrorException e) {
			assertThat(e.getStatusCode(), is(HttpStatus.FORBIDDEN));
		}
	}
}
