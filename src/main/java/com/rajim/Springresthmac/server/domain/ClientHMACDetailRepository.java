package com.rajim.Springresthmac.server.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientHMACDetailRepository  extends CrudRepository<ClientHMACDetail, Long> {

	@Query("select chd from ClientHMACDetail chd where chd.publicKey=?1")
	ClientHMACDetail loadClientByPublicKey(String username);
}
