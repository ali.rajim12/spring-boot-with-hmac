package com.rajim.Springresthmac.server.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TestRequest {

	private String message;
}
